git clone https://github.com/AmnestyTech/investigations/
cd investigations
cat */*domains.txt | sort | uniq | tee -a amensty_nso_domain_list.txt
cp amensty_nso_domain_list.txt ../amensty_nso_domain_list.txt
rm amensty_nso_domain_list.txt
cd ../
rm -r investigations
sed -i 's/^/0.0.0.0 /' amensty_nso_domain_list.txt
cd ../
