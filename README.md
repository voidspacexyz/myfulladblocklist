This is my personal AdList repository. This may or maynot be applicable to you. This blocks all forms of Meta based products and a good chunck of trackers.

### How to use: ``` bash setup.sh ``` 

### Assumptions: This script uses a default set of GNU/Linux tools like `wget` `cp` `cd` `tee` & `grep`. Please ensure these tools are available and you are connected to an internet.

### What this generates :


1. `voidspacexyz-adlist.txt` : This is the full-adlist I use in my network. This is a decently agressive list, so if you are a common internet user, I would recommend this list with caution. This script basically generate a larger list from stevenblack-adlist, Energized Pro list, DeveloperDan's agressive extended list, blocklistproject everything, and the amensty_nso_domain_list below.
2. `amensty_nso_domain_list.txt` : This list is the NSO tracker list which I keep generating regularly from the Amnesty's git repo (refer to the script). You are welcome to only use this list.


As always if you feel you are clueless, feel free to reachout via email : null@voidspace.xyz
